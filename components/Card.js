import React from 'react';
import { StyleSheet, View } from 'react-native';

const Card = props => {
return <View style={{...styles.card, ...props.style}}>{props.children}</View>
};

const styles = StyleSheet.create({
    card: {
        marginTop: 20,
        width: '90%',
        padding: 20,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        shadowOpacity: 0.25,
        elevation: 12,
        backgroundColor: 'white',
        borderRadius: 4,
    },
});

export default Card;