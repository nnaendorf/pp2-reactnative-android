import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Button, TouchableOpacity, Image, Alert } from "react-native";
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const LicenseScreen = props => {

    const [pickedFrontside, setPickedFrontside] = useState();
    const [pickedBackside, setPickedBackside] = useState();

    // Called when an image is being picked
    const pickImage = async name => {
        if (verifyPermission) {
            const image = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 3],
                quality: 0.7,
            });
            if (name === 'frontside') {
                setPickedFrontside(image.uri);
            } else {
                setPickedBackside(image.uri);
            }
        };
    };

    // Checks permission for using the camera
    const verifyPermission = async () => {
        const result = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        if (result.status != 'granted') {
            Alert.alert('Kamerazugriff wird zum Aufnehmen eines Bildes benötigt!');
            return false;
        };
        return true;
    };

    // Sends an image to the REST-Api
    const uploadImages = async () => {
        let formData = new FormData();
        formData.append(
            'frontside', 
            {
                uri: pickedFrontside,
                type: 'image/jpeg',
                name: 'frontside.jpg'
            }
        );
        formData.append(
            'backside', 
            {
                uri: pickedBackside,
                type: 'image/jpeg',
                name: 'backside.jpg',
            }
        );
        if (pickedFrontside === undefined || pickedBackside === undefined) {
            throw new Error('Wählen Sie zwei Bilder aus!');
        } else {
            const response = await fetch(
                'http://212.227.10.211:8080/pp-app-server/driverlicense',
                {
                    method: 'POST',
                    headers: {
                        'cookie': session,
                    },
                    body: formData,
                }
            )
            if (!response.ok) {
                throw new Error('Upload nicht erfolgreich!');
            }
        }
    }

    // build-Method of the react functional component
    return (
        <View style={styles.screen}>
            <View style={styles.licensesContainer}>
                <View style={styles.touchableContainer}>
                    <TouchableOpacity onPress={() => {
                        pickImage('frontside');
                    }}>
                        {!pickedFrontside ? 
                        <View style={styles.touchable}>
                            <Ionicons name='ios-camera' size={25} color='grey'/>
                            <Text style={{color: 'grey'}}>Vorderseite</Text>
                        </View> : 
                        <Image style={styles.image} source={{uri: pickedFrontside}}/>}
                    </TouchableOpacity>
                </View>
                <View style={styles.touchableContainer}>
                    <TouchableOpacity style={styles.touchable} onPress={() => {
                        pickImage('backside');
                    }}>
                        {!pickedBackside ? 
                        <View style={styles.touchable}>
                            <Ionicons name='ios-camera' size={25} color='grey'/>
                            <Text style={{color: 'grey'}}>Rückseite</Text>
                        </View> : 
                        <Image style={styles.image} source={{uri: pickedBackside}}/>}
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.buttonContainer}>
                <Button title='Einreichen' color='green' onPress={ async () => {
                    try {
                        await uploadImages();
                        props.navigation.navigate({
                            routeName: 'Home',
                        });
                    } catch (error) {
                        Alert.alert(error.message);
                    }
                }}/>
            </View>
        </View>
    );
}

LicenseScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Führerschein',
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fff',
    },
    licensesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    buttonContainer: {
        flex: 0,
        width: '100%',
        justifyContent: 'flex-end'
    },
    touchableContainer: {
        width: '85%',
        height: '30%',
        borderColor: '#dedede',
        borderWidth: 1,
        borderRadius: 5,
        elevation: 15,
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    touchable: {
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: '100%',
    },
});

export default LicenseScreen;