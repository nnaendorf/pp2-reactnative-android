import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Button, AsyncStorage, Alert } from 'react-native';
import { Button as ElementButton } from 'react-native-elements';
import { Item, HeaderButtons } from 'react-navigation-header-buttons';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

import HeaderButton from '../components/HeaderButton';
import Colors from '../constants/Colors';
import * as reminder from '../store/actions/reminders'
import { useForceUpdate } from '../components/UseForceUpdate';
import ListItem from '../components/ListItem';
import { ScrollView } from 'react-native';
import firebase from 'react-native-firebase';

const HomeScreen = props => {

    const forceUpdate = useForceUpdate();
    const dispatch = useDispatch();
    const [registered, setRegistered] = useState(true);
    const session = useSelector(state => state.auth.session);
    const fetchedReminders = useSelector(state => state.reminders.reminders);

    // Reminders that will be displayed as frequent reminders
    const displayedReminders = fetchedReminders.filter(reminder => {
        if (new Date(reminder['startTime']) > new Date()) {
            return true;
        } else {
            return false;
        }
    });

    // Sorted by frequency
    displayedReminders.sort((a, b) => {
        return new Date(a['startTime']) - new Date(b['startTime']);
    });

    // Fetches reminders everytime state changes
    useEffect(() => {
        async function fetchData() {
            await dispatch(reminder.fetchReminders(session));
        }
        fetchData();
    });

    const messageListening = async () => {
        const notificationListener = firebase.notifications().onNotification((notification) => {
          const { title, body } = notification;
          showAlert(title, body);
        });
       
        const notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
          const { title, body } = notificationOpen.notification;
          showAlert(title, body);
        });
       
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
          const { title, body } = notificationOpen.notification;
          showAlert(title, body);
        }
       
        const messageListener = firebase.messaging().onMessage((message) => {
         console.log(JSON.stringify(message));
        });
    }

    // Alert template to present error messages etc.
    const showAlert = (title, message) => {
        Alert.alert(
         title,
         message,
         [
          {text: "OK", onPress: () => console.log("OK Pressed")},
         ],
         {cancelable: false},
        );
    }

    // Called, when the ElementButton for push messages is triggered
    // Either registeres or unregisteres the device to receive push notifications
    const pushClicked = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (!enabled) {
            try {
                await firebase.messaging().requestPermission();
            } catch (error) {
                Alert.alert('Push Nachrichten nicht aktiviert - Fehlende Berechtigung');
                return;
            }
        }
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
        setRegistered(!registered);
        let formData = new FormData();
        formData.append('token', fcmToken);
        if (registered) {
            const response = await fetch(
                'http://212.227.10.211:8080/pp-app-server/register',
                {
                    method: 'POST',
                    headers: {
                        'cookie': session,
                    },
                    body: formData,
                }
            );
            if (!response.ok) {
                Alert.alert('Push Nachrichten konnten nicht aktiviert werden.');
                setRegistered(false);
                return;
            }
        } else {
            const response = await fetch(
                'http://212.227.10.211:8080/pp-app-server/unregister',
                {
                    method: 'POST',
                    headers: {
                        'cookie': session,
                    },
                    body: formData,
                }
            );
            if (!response.ok) {
                Alert.alert('Push Nachrichten konnten nicht deaktiviert werden.');
                setRegistered(true);
                return;
            }
        }
    }

    // Build method of the react functional component
    return (
        <View style={styles.screen}>
            <View style={styles.refreshContainer}>
                <ElementButton 
                    icon={{name: 'refresh'}} 
                    title='Refresh' 
                    buttonStyle={styles.refresh} 
                    titleStyle={{color: '#5c5c5c'}}
                    onPress={() => forceUpdate()}
                />
                <ElementButton 
                    icon={{name: registered ? 'check-box-outline-blank' : 'check-box'}} 
                    title='Push' 
                    buttonStyle={styles.refresh} 
                    titleStyle={{color: '#5c5c5c'}}
                    onPress={() => {pushClicked()}}
                />
            </View>
            <View style={{height: displayedReminders.length === 0 ? '0%' : '30%', marginTop: 10, alignItems: 'center'}}>
                <Text style={styles.textContainer}>{displayedReminders.length === 0 ? 'Keine anstehenden Erinnerungen' : 'Anstehende Erinnerungen'}</Text>
                <ScrollView style={styles.scrollview}>
                    {displayedReminders.map(reminder => 
                        <ListItem 
                            key={Math.random()}
                            time={reminder['allDay'] ? 
                                moment(reminder['startTime']).format('DD.MM.YYYY:') + ' Ganztags' : 
                                moment(reminder['startTime']).format('DD.MM.YYYY: HH:mm') + ' Uhr - ' + 
                                moment(reminder['endTime']).format('HH:mm') + ' Uhr'
                            }>
                            {reminder['title']}
                        </ListItem>)}
                </ScrollView>
            </View>
            <View style={styles.buttonScreen}>
                <View style={styles.buttonContainer}>
                    <Button color={Colors.primaryColor} title='Belege' onPress={() => {
                        props.navigation.navigate({
                            routeName: 'Invoice',
                        })
                    }}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button color={Colors.primaryColor} title='Führerschein' onPress={() => {
                        props.navigation.navigate({
                            routeName: 'License',
                        })
                    }}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button color={Colors.primaryColor} title='Erinnerungen' onPress={() => {
                        props.navigation.navigate({
                            routeName: 'Reminder',
                        })
                    }}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button color='#fa3939' title='Logout' onPress={() => {
                        props.navigation.navigate({
                            routeName: 'Auth',
                        })
                    }}/>
                </View>
            </View>
        </View>
    );
}

HomeScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Übersicht',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item title={'Menü'} iconName='ios-menu' onPress={() => {
                navData.navigation.toggleDrawer();
            }}></Item>
        </HeaderButtons>
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fff',
      },
      buttonScreen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      buttonContainer: {
        width: '70%',
        height: '5%',
        borderRadius: 15,
      },
      button: {
          borderRadius: 15,
      },
      refreshContainer: {
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-around',
      },
      refresh: {
        backgroundColor: 'white',
      },
      scrollview: {
          width: '100%',
      },
      textContainer: {
          alignItems: 'center',
          fontWeight: 'bold',
      }
});

export default HomeScreen;