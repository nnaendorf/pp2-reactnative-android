import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Button, ScrollView } from "react-native";
import { CalendarList, LocaleConfig } from 'react-native-calendars';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import ListItem from '../components/ListItem';

const ReminderScreen = props => {

    const reminders = useSelector(state => state.reminders.reminders);
    const events = getEvents();

    function getEvents() {
        let temp = {};
        for (let i = 0; i < reminders.length; i++) {
            temp[reminders[i]['startTime'].substring(0, 10)] = {selected: true};
        }
        return temp;
    };

    const [selectedDay, setSelectedDay] = useState();
    const [selectedDayEvents, setSelectedDayEvents] = useState([]);
    const selectDay = day => {
        setSelectedDayEvents([]);
        setSelectedDay(day.dateString);
        for (let i = 0; i < reminders.length; i++) {
            if (reminders[i]['startTime'].substring(0, 10) === day.dateString) {
                setSelectedDayEvents(oldEvents => [...oldEvents, reminders[i]]);
            }
        }
        setSelectedDayEvents(unsortedEvents => unsortedEvents.sort((a, b) => {
            return new Date(b.startTime) - new Date(a.startTime)
        }));
    }

    useEffect(() => {
        getEvents();
    });

    LocaleConfig.locales['de'] = {
        monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthNamesShort: ['Jan', 'Feb', 'März', 'Apr', 'Mai', 'Juni', 'Juli', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    }
    LocaleConfig.defaultLocale = 'de';

    // build-Method for the react functional component
    return (
        <View style={styles.screen}>
            <View style={styles.calendar}>
                <CalendarList
                    horizontal={true}
                    pagingEnabled={true}
                    onDayPress={(day)=>{}}
                    firstDay={1}
                    onDayPress={(day) => selectDay(day)}
                    markedDates={events}
                />
            </View>
            <ScrollView style={styles.scrollView}>
                <View>
                    <Text style={styles.title}>
                        {!selectedDay ? '' : 'Erinnerungen am ' + moment(selectedDay).format('DD.MM.YYYY')}
                    </Text>
                    {!selectedDayEvents ? <Text></Text> : selectedDayEvents.map(event => 
                    <ListItem 
                        key={Math.random()} 
                        time={event['allDay'] ? 
                            'Ganztags' : 
                            moment(event['startTime']).format('HH:mm') + ' Uhr - ' + 
                            moment(event['endTime']).format('HH:mm') + ' Uhr'
                        }>
                        {event.title}
                    </ListItem>)}
                </View>
            </ScrollView>
        </View>
    );
}

ReminderScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Erinnerungen',
        headerRight: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item title='Add' iconName={'ios-add'} onPress={() => {
                navData.navigation.navigate({
                    routeName: 'NewReminder',
                })
            }}/>
        </HeaderButtons>
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    calendar: {
        // Corresponds to the needed height of the calendar
        height: 360,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    }, 
    scrollView: {
        width: '100%',
    }
});

export default ReminderScreen;