import React, { useReducer, useState } from 'react';
import { useSelector } from 'react-redux';
import { StyleSheet, View, Text, ScrollView, TextInput, Button, Alert, Image } from "react-native";
import { FloatingAction } from "react-native-floating-action";
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import Card from '../components/Card';
import Colors from '../constants/Colors';

// Options, when selecting an image
const actions = [
    {
      text: "Photo",
      icon: <Ionicons name='ios-camera' size={25} color='white'/>,
      name: "bt_photo",
      position: 2,
      color: Colors.primaryColor,
    },
    {
      text: "Galerie",
      icon: <Ionicons name='ios-images' size={25} color='white'/>,
      name: "bt_galery",
      position: 1,
      color: Colors.primaryColor,
    },
  ];

// Reducer for useReducer
const formReducer = (state, action) => {
    if (action.type === 'UPDATE') {
        const newValues = {
            ...state.inputValues, 
            [action.input]: action.value,
        };
        const newValidities = {
            ...state.inputValidities,
            [action.input]: action.isValid,
        };
        const newTouched = {
            ...state.touched,
            [action.input]: action.touched,
        }
        let newFormValidation = true;
        for (const key in newValidities) {
            if (newValidities[key] === false) newFormValidation = false;
        }
        return {
            inputValidities: newValidities,
            inputValues: newValues,
            touched: newTouched,
            formIsValid: newFormValidation,
        }
    }
    return state;
};

const InvoiceScreen = props => {

    const session = useSelector(state => state.auth.session);
    const [pickedImage, setPickedImage] = useState();
    // useReducer = useState, but simpler and more effective
    const [formState, dispatch] = useReducer(formReducer, {
        inputValues: {
            title: '',
            description: '',
            price: '',
        },
        inputValidities: {
            title: false,
            description: false,
            price: false,
        },
        touched: {
            title: false,
            description: false,
            price: false,
        },  
        formIsValid: false,
    });

    // Called, whenever the form changes
    const changeHandler = (value, identifier) => {
        let isValid = false;
        if (value.trim().length > 0) {
            isValid = true;
        }
        if (identifier === 'price') {
            value = value.replace(/[^0-9]/g, '');
        }
        dispatch({
            type: 'UPDATE',
            value: value,
            isValid: isValid,
            touched: true,
            input: identifier,
        });
    };

    const validate = () => {
        if (!formState.formIsValid) {
            throw new Error('Überprüfen Sie Ihre Eingabefelder!');
        };
    };

    // Called when an image is being picked
    const pickImage = async name => {
        if (name === 'bt_photo') {
            if (verifyPermission) {
                const image = await ImagePicker.launchCameraAsync({
                    allowsEditing: true,
                    aspect: [4, 3],
                    quality: 0.7,
                });
                setPickedImage(image.uri);
            };
        } else {
            if (verifyPermission) {
                const image = await ImagePicker.launchImageLibraryAsync({
                    allowsEditing: true,
                    aspect: [4, 3],
                    quality: 0.7,
                });
                setPickedImage(image.uri);
            }
        };
    };

    // Get permission for the camera / galerry
    const verifyPermission = async () => {
        const result = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        if (result.status != 'granted') {
            Alert.alert('Kamerazugriff wird zum Aufnehmen eines Bildes benötigt!');
            return false;
        };
        return true;
    };

    // Uploading an image to the REST-Api
    const uploadImage = async () => {
        if (pickedImage === undefined) {
            throw new Error('Wählen Sie ein Bild aus!')
        } else {
            let formData = new FormData();
            formData.append(
                'invoice', 
                {
                    uri: pickedImage,
                    type: 'image/jpeg',
                    name: 'invoice.jpg',
                }
            );
            formData.append('title', formState.inputValues.title);
            formData.append('description', formState.inputValues.description);
            formData.append('price', formState.inputValues.price);
            const response = await fetch(
                'http://212.227.10.211:8080/pp-app-server/invoices',
                {
                    method: 'POST',
                    headers: {
                        'cookie': session,
                    },
                    body: formData,
                }
            );
            if (!response.ok) {
                throw new Error('Upload ist fehlgeschlagen!');
            }
        }
    }

    // build-Method of the react functional component
    return (
        <View style={styles.screen}>
        <ScrollView style={styles.scrollview}>
            <View style={styles.card}> 
                <Card>
                    <Text style={styles.label}>Titel</Text>
                    <TextInput 
                        style={styles.input}
                        value={formState.inputValues.title}
                        onChangeText={value => changeHandler(value, 'title')}
                        returnKeyType='next'
                        maxLength={30}
                    />
                    {!formState.inputValidities.title && formState.touched.title ? 
                    <Text style={styles.sub}>Validen Titel eingeben</Text> : <Text/>}
                    <Text style={styles.label}>Beschreibung</Text>
                    <TextInput
                        style={styles.input}
                        value={formState.inputValues.description}
                        onChangeText={value => changeHandler(value, 'description')}
                        maxLength={180}
                        multiline={true}
                    />
                    {!formState.inputValidities.description && formState.touched.description ? 
                    <Text style={styles.sub}>Valide Beschreibung eingeben</Text> : <Text/>}
                    <Text style={styles.label}>Preis (€)</Text>
                    <TextInput
                        style={styles.input}
                        value={formState.inputValues.price}
                        onChangeText={value => changeHandler(value, 'price')}
                        keyboardType="decimal-pad"
                    />
                    {!formState.inputValidities.price && formState.touched.price ? 
                    <Text style={styles.sub}>Validen Preis eingeben</Text> : <Text/>}
                    <View style={styles.buttonContainer, {alignSelf: 'flex-end'}}>
                        <Button color='green' title='EINREICHEN' onPress={ async () => {
                            try {
                                validate();
                                await uploadImage();
                                props.navigation.navigate({
                                    routeName: 'Home',
                                });
                            } catch (error) {
                                Alert.alert(error.message);
                            }
                        }}/>
                    </View>
                </Card>
                <View style={styles.imageContainer}>
                    {!pickedImage ? <Text>Bitte ein Bild vom Beleg auswählen</Text>
                    : <Image style={styles.image} source={{uri: pickedImage}}/>}
                </View>
            </View>
        </ScrollView>
        <FloatingAction
            color={Colors.primaryColor}
            actions={actions}
            onPressItem={name => {
                pickImage(name);
            }}
        />
        </View>
    );
}

InvoiceScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Belege',
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    scrollview: {
        width: '95%',
    },
    card: {
        alignItems: 'center',
    },
    label: {
        marginBottom: 5,
        fontWeight: 'bold',
        fontSize: 13,
    },
    input: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        marginBottom: 0,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    buttonContainer: {
        width: '30%',
    },
    sub: {
        marginBottom: 15,
        fontSize: 11,
        color: 'red',
    },
    image: {
        width: '100%',
        height: '100%',
    },
    imageContainer: {
        width: '89%',
        height: 250,
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default InvoiceScreen;