import React, { useReducer, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TextInput, Button, CheckBox, Switch, Alert } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import Card from '../components/Card';
import Colors from '../constants/Colors';

// Reducer for useReducer
const formReducer = (state, action) => {
    if (action.type === 'UPDATE') {
        const newValues = {
            ...state.inputValues, 
            [action.input]: action.value,
        };
        const newValidities = {
            ...state.inputValidities,
            [action.input]: action.isValid,
        };
        const newTouched = {
            ...state.touched,
            [action.input]: action.touched,
        }
        let newFormValidation = true;
        for (const key in newValidities) {
            if (newValidities[key] === false) newFormValidation = false;
        }
        return {
            inputValidities: newValidities,
            inputValues: newValues,
            touched: newTouched,
            formIsValid: newFormValidation,
        }
    }
    return state;
};

const NewReminderScreen = props => {

    const session = useSelector(state => state.auth.session);
    const [allDay, setAllDay] = useState(false);
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [isStartTimePickerVisible, setIsStartTimePickerVisible] = useState(false);
    const [isEndTimePickerVisible, setIsEndTimePickerVisible] = useState(false);
    // useReducer = useState, but simpler and more efficient
    const [formState, dispatchFS] = useReducer(formReducer, {
        inputValues: {
            title: '',
            description: '',
            date: '',
            startTime: '',
            endTime: ''
        },
        inputValidities: {
            title: false,
            description: false,
            date: false,
            startTime: false,
            endTime: false,
        },
        touched: {
            title: false,
            description: false,
            date: false,
            startTime: false,
            endTime: false,
        },  
        formIsValid: false,
    });

    // Called, whenever the form changes
    const changeHandler = (value, identifier) => {
        let isValid = false;
        if (value.trim().length > 0) {
            isValid = true;
        }
        if (identifier === 'startTime' && new Date(value) > new Date(formState.inputValues.endTime)) {
            dispatchFS({
                type: 'UPDATE',
                value: value,
                isValid: isValid,
                touched: true,
                input: 'endTime',
            });
        }
        if (identifier === 'endTime' && new Date(formState.inputValues.startTime) > new Date(value)) {
            dispatchFS({
                type: 'UPDATE',
                value: value,
                isValid: isValid,
                touched: true,
                input: 'startTime',
            });
        }
        dispatchFS({
            type: 'UPDATE',
            value: value,
            isValid: isValid,
            touched: true,
            input: identifier,
        });
    };

    const validate = () => {
        if (!formState.formIsValid) {
            throw new Error('Überprüfen Sie Ihre Eingabefelder!');
        };
    };

    const showPicker = picker => {
        if (picker === 'date') setIsDatePickerVisible(true);
        if (picker === 'starttime') setIsStartTimePickerVisible(true);
        if (picker === 'endtime') setIsEndTimePickerVisible(true);
    }

    const hidePicker = picker => {
        if (picker === 'date') setIsDatePickerVisible(false);
        if (picker === 'starttime') setIsStartTimePickerVisible(false);
        if (picker === 'endtime') setIsEndTimePickerVisible(false);
    }

    const confirmDate = date => {
        hidePicker('date');
        changeHandler(new Date(date).toISOString(), 'date');
    }

    const confirmStartTime = time => {
        hidePicker('starttime');
        changeHandler(new Date(time).toISOString(), 'startTime');
    }

    const confirmEndTime = time => {
        hidePicker('endtime');
        changeHandler(new Date(time).toISOString(), 'endTime');
    }

    // Builds the date string to fit the format in the db
    const stringBuilder = (date, time) => {
        if (new Date(time).getHours() < 9) {
            return date.substring(0, 11) + '0' + new Date(time).getHours() + time.substring(13, 17) + '00.000+02:00';
        } else {
            return date.substring(0, 11) + new Date(time).getHours() + time.substring(13, 17) + '00.000+02:00';
        }
    }

    // Called when uploading a new reminder, using FormData
    const uploadReminder = async (session, title, description, date, startTime, endTime, allDay) => {
        const startTimeString = stringBuilder(date, startTime);
        const endTimeString = stringBuilder(date, endTime);
        let formData = new FormData();
        formData.append('id', Math.random().toString());
        formData.append('userId', 'x');
        formData.append('title', title);
        formData.append('description', description);
        formData.append('startTime', startTimeString);
        formData.append('endTime', endTimeString);
        formData.append('allDay', allDay);
        const response = await fetch(
            'http://212.227.10.211:8080/pp-app-server/reminder',
            {
                method: 'POST',
                headers: {
                    'cookie': session,
                },
                body: formData,
            }
        )
        if (!response.ok) {
            throw new Error('Upload nicht erfolgreich!');
        }
    }

    // build-Method of the react functional component
    return (
        <View style={styles.screen}>
            <ScrollView style={styles.scrollview}>
                <View style={styles.card}>
                    <Card>
                        <Text style={styles.label}>Titel</Text>
                        <TextInput 
                            style={styles.input}
                            value={formState.inputValues.title}
                            onChangeText={value => changeHandler(value, 'title')}
                            returnKeyType='next'
                            maxLength={30}
                        />
                        {!formState.inputValidities.title && formState.touched.title ? 
                        <Text style={styles.sub}>Validen Titel eingeben</Text> : <Text/>}
                        <Text style={styles.label}>Beschreibung</Text>
                        <TextInput
                            style={styles.input}
                            value={formState.inputValues.description}
                            onChangeText={value => changeHandler(value, 'description')}
                            maxLength={180}
                            multiline={true}
                        />
                        {!formState.inputValidities.description && formState.touched.description ? 
                        <Text style={styles.sub}>Valide Beschreibung eingeben</Text> : <Text/>}
                        <View style={styles.row}>
                            <View style={styles.column}>
                                <Button color={Colors.primaryColor} title='Datum' onPress={() => showPicker('date')}/>
                                {formState.inputValues.date === '' ? <Text/> :
                                <Text style={styles.sub, {color: 'grey'}}>{moment(formState.inputValues.date).format('DD.MM.YYYY')}</Text>}
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisible}
                                    mode='date'
                                    onConfirm={confirmDate}
                                    onCancel={() => hidePicker('date')}
                                    locale='de_DE'
                                    is24Hour={true}
                                />
                            </View>
                            <View style={styles.column}>
                                <Text style={{color: 'grey'}}>Ganztägig</Text>
                                <Switch
                                    trackColor={{true: Colors.primaryColor}}
                                    thumbColor='white'
                                    value={allDay}
                                    onValueChange={newValue => setAllDay(newValue)}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.column}>
                                <Button color={Colors.primaryColor} title='Start' onPress={() => showPicker('starttime')}/>
                                {formState.inputValues.startTime === '' ? <Text/> :
                                <Text style={styles.sub, {color: 'grey'}}>{moment(formState.inputValues.startTime).format('HH:mm')} Uhr</Text>}
                                <DateTimePickerModal
                                    isVisible={isStartTimePickerVisible}
                                    mode='time'
                                    locale='de_DE'
                                    onConfirm={confirmStartTime}
                                    onCancel={() => hidePicker('starttime')}
                                    is24Hour={true}
                                />
                            </View>
                            <View style={styles.column}>
                                <Button color={Colors.primaryColor} title='Ende' onPress={() => showPicker('endtime')}/>
                                {formState.inputValues.endTime === '' ? <Text/> :
                                <Text style={styles.sub, {color: 'grey'}}>{moment(formState.inputValues.endTime).format('HH:mm')} Uhr</Text>}
                                <DateTimePickerModal
                                    isVisible={isEndTimePickerVisible}
                                    mode='time'
                                    onConfirm={confirmEndTime}
                                    onCancel={() => hidePicker('endtime')}
                                    locale='de_DE'
                                    is24Hour={true}
                                />
                            </View>
                        </View>
                        <Button title='EINREICHEN' color='green' onPress={ async () => {
                            try {
                                validate();
                                await uploadReminder(
                                    session,
                                    formState.inputValues.title,
                                    formState.inputValues.description,
                                    formState.inputValues.date,
                                    formState.inputValues.startTime,
                                    formState.inputValues.endTime,
                                    allDay,
                                );
                                props.navigation.navigate({
                                    routeName: 'Home',
                                });
                            } catch (error) {
                                Alert.alert(error.message);
                            }
                        }}/>
                    </Card>
                </View>
            </ScrollView>
        </View>
    );
}

NewReminderScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Neue Erinnerung',
    }
}

const styles = StyleSheet.create({
    screen: {
        alignItems: 'center',
        flex: 1,
        backgroundColor: '#fff',
    },
    scrollview: {
        width: '95%',
    },
    card: {
        alignItems: 'center',
    },
    label: {
        marginBottom: 5,
        fontWeight: 'bold',
        fontSize: 13,
    },
    input: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        marginBottom: 0,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    sub: {
        marginBottom: 15,
        fontSize: 11,
        color: 'red',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
    },
    column: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '30%'
    },
});

export default NewReminderScreen;