import React, { useState, useReducer } from 'react'
import { Text, StyleSheet, Button, View, TextInput, ScrollView, Alert, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Card from '../components/Card';
import Colors from '../constants/Colors';
import * as auth from '../store/actions/auth';

const AuthScreen = props => {

    // Setting state and validators for the form
    const dispatch = useDispatch();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [emailIsValid, setEmailIsValid] = useState(false);
    const [passwordIsValid, setPasswordIsValid] = useState(false);
    const [emailTouched, setEmailTouched] = useState(false);
    const [passwordTouched, setPasswordTouched] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    // Called when form changes
    const onChangeHandler = (value, name) => {
        if (name === 'email') {
            setEmail(value);
            setEmailTouched(true);
            setEmailIsValid(false);
            if (value.includes('@')) {
                setEmailIsValid(true);
            }
        }
        if (name === 'password') {
            setPassword(value);
            setPasswordTouched(true);
            setPasswordIsValid(false);
            if (value.trim().length > 10) {
                setPasswordIsValid(true);
            }
        }
    }

    // build function from the react functional component
    return (
        <View style={styles.screen}>
            <Card>
                <ScrollView>
                    <Text style={styles.label}>E-Mail</Text>
                    <TextInput
                        style={styles.input}
                        value={email}
                        onChangeText={value => onChangeHandler(value, 'email')}
                        returnKeyType='next'
                        maxLength={30}
                    />
                    {!emailIsValid && emailTouched ? 
                    <Text style={styles.sub}>Keine valide E-Mail (muss @ enthalten)</Text> : <Text/>}
                    <Text style={styles.label}>Password</Text>
                    <TextInput
                        style={styles.input}
                        value={password}
                        onChangeText={value => onChangeHandler(value, 'password')}
                        returnKeyType='next'
                        maxLength={30}
                        secureTextEntry={true}
                    />
                    {!passwordIsValid && passwordTouched ? 
                    <Text style={styles.sub}>Kein valides Password (min. 10 Zeichen)</Text> : <Text/>}
                </ScrollView>
            </Card>
            <View style={styles.buttonContainer}>
                {isLoading ? 
                <ActivityIndicator size='large' color={Colors.primaryColor}/> :
                <Button color={Colors.primaryColor} title='Login' onPress={ async () => {
                    try {
                        setIsLoading(true);
                        await dispatch(auth.login(email, password));
                        setIsLoading(false);
                        props.navigation.navigate({
                            routeName: 'Home',
                        });
                    } catch (error) {
                        setIsLoading(false);
                        Alert.alert('Etwas ist schiefgelaufen!');
                    }
                }}/>}
            </View>
        </View>
    );
}

AuthScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Login',
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    label: {
        marginBottom: 5,
        fontSize: 15,
    },
    input: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        marginBottom: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    buttonContainer: {
        marginTop: 50,
        width: '30%',
    },
    sub: {
        fontSize: 12,
        color: 'red'
    }
});

export default AuthScreen;