// Redux: Action for auth object
import base64 from 'react-native-base64';

export const login = (email, password) => {
    return async dispatch => {
        const stringToEncode = email + ':' + password;
            const base64encodedAuth = base64.encode(stringToEncode);
            const response = await fetch(
                'http://212.227.10.211:8080/pp-app-server/authentication',
                {
                    method: 'GET',
                    headers: {
                        Authorization: 'Basic ' + base64encodedAuth,
                    },
                }
            );
            if (!response.ok) {
                throw new Error('Login nicht erfolgreich!');
            }
            if (response.headers['map']['set-cookie']) {
                console.log('Cookie found!');
                let index = response.headers['map']['set-cookie'].indexOf(';');
                const session = response.headers['map']['set-cookie'].substring(0, index);
                dispatch({ type: 'LOGIN', session: session})
            }
    }
}

