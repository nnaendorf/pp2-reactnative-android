// Redux: Reducer from reminders object 
const initialState = {
    reminders: [],
}
let lastReminders = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH':
            if (JSON.stringify(lastReminders) != JSON.stringify(action.reminders)) {
                lastReminders = action.reminders;
                return {reminders: action.reminders};
            } else {
                return state;
            }
        default: 
            return state;
    }
}