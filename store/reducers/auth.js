// Redux: Reducer from auth object
const user = {
    session: ''
}

export default (state = user, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {session: action.session};
        default:
            return state;
    }
}